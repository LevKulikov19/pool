﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Pool
{
    class PumpModel : IDrawing, IDisposable
    {
        public delegate void PumpModelEvent(object sender, uint volume);
        public event PumpModelEvent PopWhater;

        Thread thread;
        bool isThreadCanceled = false;

        bool isWork;
        public bool IsWork
        {
            get => isWork;
            set
            {
                isWork = value;
            }
        }
        static uint number = 0;
        public uint Number
        {
            get;
            set;
        }

        public Point Location
        {
            get;
            set;
        }

        public Size Size
        {
            get;
            set;
        }

        Rectangle buttonRectangle;
        Color colorButtonOn = ColorTranslator.FromHtml("#4EAD15");
        Color colorButtonOff = ColorTranslator.FromHtml("#BB0000");

        object imageBackgroundLock = new object();
        Image imageBackground;

        public PumpModel()
        {
            number++;
            Number = number;
            imageBackground = ResourceMain.BackgroundPump;
            thread = new Thread(StartPump);
            thread.Name = "Pump\n№" + Number.ToString();
            thread.Start();
            IsWork = false;
        }

        private void StartPump()
        {
            while(!isThreadCanceled)
            {
                if (IsWork) PopWhater(this, 20);
                Thread.Sleep(1000);
            }
        }

        public void Draw(Graphics graphics)
        {
            Rectangle rectangleBackground = new Rectangle(Location, Size);
            lock (imageBackgroundLock)
            {
                graphics.DrawImage(imageBackground, rectangleBackground);
            }

            DrawTitle(graphics);
            DrawButton(graphics);
        }

        private void DrawTitle(Graphics graphics)
        {
            string name = "Насос\n№" + Number.ToString();
            Font nameFont = new Font("Roboto", 14.0F);
            Point namePosition = Location;
            namePosition.Offset((int)(graphics.MeasureString(name, nameFont).Width / 2), 0);
            namePosition.Offset((int)(((Size.Width + 10) - (graphics.MeasureString(name, nameFont).Width)) / 2), 15);
            Brush brushFont = new SolidBrush(Color.White);
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            graphics.DrawString(name, nameFont, brushFont, namePosition, format);
        }

        private void DrawButton(Graphics graphics)
        {
            string buttonText;
            Brush buttonPen;
            if (IsWork)
            {
                buttonPen = new SolidBrush(colorButtonOff);
                buttonText = "Выкл";
            }
            else
            {
                buttonPen = new SolidBrush(colorButtonOn);
                buttonText = "Вкл";
            }

            buttonRectangle = new Rectangle(Location.X + 15, Location.Y + 75, 80, 32);
            graphics.FillRectangle(buttonPen, buttonRectangle);

            Font buttonFont = new Font("Roboto", 12.0F);
            Point buttonTextPosition = new Point(buttonRectangle.X + buttonRectangle.Width / 2,
                                                 buttonRectangle.Y + buttonRectangle.Height / 2);
            Brush buttonTextBrushFont = new SolidBrush(Color.White);
            StringFormat buttonTextFormat = new StringFormat();
            buttonTextFormat.Alignment = StringAlignment.Center;
            buttonTextFormat.LineAlignment = StringAlignment.Center;
            graphics.DrawString(buttonText, buttonFont, buttonTextBrushFont, buttonTextPosition, buttonTextFormat);
        }

        public void Click(object sender, MouseEventArgs e)
        {
            if (Inside(e.Location, buttonRectangle)) IsWork = !IsWork;
        }

        private bool Inside(Point position, Rectangle rectangle)
        {
            Point positionRelative = position;
            positionRelative.Offset(-rectangle.X, -rectangle.Y);
            return (positionRelative.X <= rectangle.Width && positionRelative.X >= 0
                && positionRelative.Y <= rectangle.Height && positionRelative.Y >= 0);
        }

        public void Stop (object sender, uint level)
        {
            IsWork = false;
        }

        public void Start(object sender, uint level)
        {
            IsWork = true;
        }

        public void Dispose()
        {
            IsWork = false;
            isThreadCanceled = true;
            imageBackground.Dispose();
        }
    }
}
