﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pool
{
    public partial class FormMain : Form
    {
        List<IDrawing> draws;

        PoolModel pool;
        CraneModel crane;
        PumpModel[] pumps;

        object imageBackgroundLock = new object();
        Image imageBackground;

        public FormMain()
        {
            draws = new List<IDrawing>();

            initPool();
            initCrane();
            initPumps();
            foreach (PumpModel item in pumps)
            {
                pool.MinLevel += item.Stop;
                pool.MinLevel += Pool_MinLevel;
                pool.MaxOverLevel += item.Start;
                pool.MinOverLevel += item.Stop;
                pool.MinOverLevel += Pool_MinOverLevel; ;
            }
            pool.MaxLevel += crane.Stop;
            pool.MaxLevel += Pool_MaxLevel;

            imageBackground = ResourceMain.Background;
            InitializeComponent();
        }

        private void Pool_MaxLevel(object sender, uint level)
        {
            foreach (PumpModel item in pumps)
            {
                pool.MinLevel += item.Stop;
            }
        }

        private void Pool_MinOverLevel(object sender, uint level)
        {
            foreach (PumpModel item in pumps)
            {
                pool.MinLevel += item.Stop;
            }
        }

        private void Pool_MinLevel(object sender, uint level)
        {
            foreach (PumpModel item in pumps)
            {
                pool.MinLevel -= item.Stop;
            }
        }

        private void initPool ()
        {
            pool = new PoolModel();
            pool.Size = new Size(522, 211);
            pool.Location = new Point(320, 380);
            draws.Add(pool);
        }

        private void initCrane()
        {
            crane = new CraneModel();
            crane.Size = new Size(100, 117);
            crane.Location = new Point(1115, 558);
            crane.PushWhater += pool.pushWater;
            crane.PushWhater += FormMain_PopWhater_Redraw;
            MouseClick += crane.Click;
            draws.Add(crane);
        }

        private void initPumps()
        {
            pumps = new PumpModel[5];
            for (int i = 0; i < pumps.Length; i++)
            {
                pumps[i] = new PumpModel();
                pumps[i].PopWhater += pool.popWater;
                pumps[i].PopWhater += FormMain_PopWhater_Redraw;
                pumps[i].Size = new Size(100, 117);
                MouseClick += pumps[i].Click;
                draws.Add(pumps[i]);
            }

            pumps[0].Location = new Point(43, 594);
            
            pumps[1].Location = new Point(166, 594);

            pumps[2].Location = new Point(289, 594);

            pumps[3].Location = new Point(412, 594);

            pumps[4].Location = new Point(535, 594);
        }

        private void FormMain_PopWhater_Redraw(object sender, uint volume)
        {
            Draw();
        }

        private void FormMain_Paint(object sender, PaintEventArgs e)
        {
            Draw();
        }

        private void Draw ()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;

            lock(imageBackgroundLock)
            {
                gPaint.DrawImage(imageBackground, ClientRectangle);
            }

            foreach (IDrawing item in draws)
            {
                item.Draw(gPaint);
            }

            bufferedGraphics.Render(graphicsPaintSpace);
            bufferedGraphics.Dispose();
            gPaint.Dispose();
        }

        private void FormMain_Click(object sender, EventArgs e)
        {

        }

        private void FormMain_MouseClick(object sender, MouseEventArgs e)
        {
            Draw();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (PumpModel pump in pumps)
            {
                pump.PopWhater -= pool.popWater;
                pump.PopWhater -= FormMain_PopWhater_Redraw;
                pump.Dispose();
            }
            crane.Dispose();
            pool.Dispose();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {

        }
    }
}
