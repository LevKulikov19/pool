﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Pool
{
    interface IDrawing
    {
        void Draw(Graphics graphics);
        void Click(object sender, MouseEventArgs e);
    }
}
