﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Pool
{
    class PoolModel : IDrawing, IDisposable
    {
        public delegate void PoolModelEvent(object sender, uint level);
        public event PoolModelEvent MaxLevel;
        public event PoolModelEvent MaxOverLevel;
        public event PoolModelEvent MinLevel;
        public event PoolModelEvent MinOverLevel;

        public Point Location
        {
            get;
            set;
        }

        public Size Size
        {
            get;
            set;
        }

        uint volumeWhaterPool = 2000;
        uint volumeCurrentWhaterPool=1000;
        public uint volumeMinWhaterPool = 500;
        public uint volumeMaxWhaterPool = 1500;
        int VolumeCurrentWhaterPool
        {
            get => (int)volumeCurrentWhaterPool;
            set
            {
                if (value < volumeWhaterPool) volumeCurrentWhaterPool = (uint)value;
                else volumeCurrentWhaterPool = volumeWhaterPool;
                if (value <= 0) volumeCurrentWhaterPool = 1;
                if ((volumeCurrentWhaterPool >= volumeMaxWhaterPool) && (volumeCurrentWhaterPool <= volumeMaxWhaterPool+99)) if (MaxLevel != null) MaxLevel(this, volumeCurrentWhaterPool);
                if (volumeCurrentWhaterPool <= volumeMinWhaterPool && (volumeCurrentWhaterPool >= volumeMinWhaterPool - 19)) if (MinLevel != null) MinLevel(this, volumeCurrentWhaterPool);
                if (volumeCurrentWhaterPool >= volumeWhaterPool) if (MaxOverLevel != null) MaxOverLevel(this, volumeCurrentWhaterPool);
                if (volumeCurrentWhaterPool <= 1) if (MinOverLevel != null) MinOverLevel(this, volumeCurrentWhaterPool);
            }
        }

        object imageBackgroundLock = new object();
        Image imageBackground;

        public PoolModel()
        {
            imageBackground = ResourceMain.BackgroundPool;
        }

        public void pushWater (object sender, uint volume)
        {
            VolumeCurrentWhaterPool += (int)volume;
        }

        public void popWater(object sender, uint volume)
        {
            VolumeCurrentWhaterPool -= (int)volume;
        }

        public void Draw(Graphics graphics)
        {
            Rectangle rectangleBackground = new Rectangle(Location, Size);
            lock (imageBackgroundLock)
            {
                graphics.DrawImage(imageBackground, rectangleBackground);
            }

            Size sizeWhater = new Size(430, (int)(190 * VolumeCurrentWhaterPool / volumeWhaterPool));
            Point posWather = new Point(77, 190- sizeWhater.Height);
            posWather.Offset(Location);
            Rectangle rectangleWather = new Rectangle(posWather, sizeWhater);
            Brush brushWather = new SolidBrush(Color.FromArgb(100,16,137,205));
            graphics.FillRectangle(brushWather, rectangleWather);
        }

        public void Click(object sender, MouseEventArgs e)
        {
            return;
        }

        public void Dispose()
        {
            imageBackground.Dispose();
        }
    }
}
