﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Pool
{
    class CraneModel : IDrawing, IDisposable
    {
        public delegate void CraneModelEvent(object sender, uint volume);
        public event CraneModelEvent PushWhater;

        public Point Location
        {
            get;
            set;
        }

        public Size Size
        {
            get;
            set;
        }

        uint volume = 10;

        Thread thread;
        bool isThreadCanceled = false;
        bool isWork;
        public bool IsWork
        {
            get => isWork;
            set
            {
                isWork = value;
            }
        }

        object imageBackgroundLock = new object();
        Image imageBackground;

        Rectangle buttonRectangle;
        Rectangle leftControlRectangle;
        Rectangle rightControlRectangle;
        Color colorButtonOn = ColorTranslator.FromHtml("#4EAD15");
        Color colorButtonOff = ColorTranslator.FromHtml("#BB0000");

        public CraneModel()
        {
            imageBackground = ResourceMain.BackgroundСrane;
            thread = new Thread(StartCrane);
            thread.Name = "Crane";
            thread.Start();
            IsWork = false;

        }

        private void StartCrane()
        {
            while (!isThreadCanceled)
            {
                if (isWork) if (PushWhater != null) PushWhater(this, volume);
                Thread.Sleep(1000);
            }
        }

        public void Draw(Graphics graphics)
        {
            Rectangle rectangleBackground = new Rectangle(Location, Size);
            lock (imageBackgroundLock)
            {
                graphics.DrawImage(imageBackground, rectangleBackground);
            }
            DrawCotrol(graphics);
            DrawButton(graphics);
        }

        private void DrawCotrol (Graphics graphics)
        {
            string name = volume.ToString();
            Font nameFont = new Font("Roboto", 12.0F);
            Point namePosition = Location;
            namePosition.Offset((int)(graphics.MeasureString(name, nameFont).Width / 2), 0);
            namePosition.Offset((int)(((Size.Width + 10) - (graphics.MeasureString(name, nameFont).Width)) / 2), 25);
            Brush brushFont = new SolidBrush(Color.White);
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            graphics.DrawString(name, nameFont, brushFont, namePosition, format);


            Brush brushControls = new SolidBrush(Color.Gray);

            Point[] leftContorlPoints = new Point[3];
            leftContorlPoints[0] = namePosition;
            leftContorlPoints[0].Offset(-(int)graphics.MeasureString(name, nameFont).Width / 2, 0);
            leftContorlPoints[1] = leftContorlPoints[0];
            leftContorlPoints[1].Offset(0, (int)graphics.MeasureString(name, nameFont).Height);
            int middleLeftContorlPoint = (leftContorlPoints[1].Y - leftContorlPoints[0].Y) / 2;
            leftContorlPoints[2] = leftContorlPoints[0];
            leftContorlPoints[2].Offset(-middleLeftContorlPoint, middleLeftContorlPoint);
            graphics.FillPolygon(brushControls, leftContorlPoints);
            leftControlRectangle = new Rectangle(leftContorlPoints[0].X - middleLeftContorlPoint, leftContorlPoints[0].Y,
                middleLeftContorlPoint, leftContorlPoints[1].Y - leftContorlPoints[0].Y);

            Point[] rightContorlPoints = new Point[3];
            rightContorlPoints[0] = new Point((int)(namePosition.X + graphics.MeasureString(name, nameFont).Width),
                                              namePosition.Y);
            rightContorlPoints[0].Offset(-(int)graphics.MeasureString(name, nameFont).Width / 2, 0);
            rightContorlPoints[1] = rightContorlPoints[0];
            rightContorlPoints[1].Offset(0, (int)graphics.MeasureString(name, nameFont).Height);
            int middleRightContorlPoint = (rightContorlPoints[1].Y - rightContorlPoints[0].Y) / 2;
            rightContorlPoints[2] = rightContorlPoints[0];
            rightContorlPoints[2].Offset(middleRightContorlPoint, middleRightContorlPoint);
            graphics.FillPolygon(brushControls, rightContorlPoints);
            rightControlRectangle = new Rectangle(rightContorlPoints[0].X, rightContorlPoints[0].Y,
                middleRightContorlPoint, rightContorlPoints[1].Y - rightContorlPoints[0].Y);
        }

        private void DrawButton(Graphics graphics)
        {
            string buttonText;
            Brush buttonPen;
            if (IsWork)
            {
                buttonPen = new SolidBrush(colorButtonOff);
                buttonText = "Выкл";
            }
            else
            {
                buttonPen = new SolidBrush(colorButtonOn);
                buttonText = "Вкл";
            }

            buttonRectangle = new Rectangle(Location.X + 15, Location.Y + 75, 80, 32);
            graphics.FillRectangle(buttonPen, buttonRectangle);

            Font buttonFont = new Font("Roboto", 12.0F);
            Point buttonTextPosition = new Point(buttonRectangle.X + buttonRectangle.Width / 2,
                                                 buttonRectangle.Y + buttonRectangle.Height / 2);
            Brush buttonTextBrushFont = new SolidBrush(Color.White);
            StringFormat buttonTextFormat = new StringFormat();
            buttonTextFormat.Alignment = StringAlignment.Center;
            buttonTextFormat.LineAlignment = StringAlignment.Center;
            graphics.DrawString(buttonText, buttonFont, buttonTextBrushFont, buttonTextPosition, buttonTextFormat);
        }

        public void Click(object sender, MouseEventArgs e)
        {
            if (Inside(e.Location, buttonRectangle)) IsWork = !IsWork;
            if (Inside(e.Location, leftControlRectangle)) if (volume > 0) volume -= 10;
            if (Inside(e.Location, rightControlRectangle)) if (volume < 100) volume += 10; 
        }

        private bool Inside (Point position, Rectangle rectangle)
        {
            Point positionRelative = position;
            positionRelative.Offset(-rectangle.X, -rectangle.Y);
            return (positionRelative.X <= rectangle.Width && positionRelative.X >= 0
                && positionRelative.Y <= rectangle.Height && positionRelative.Y >= 0);
        }

        public void Stop(object sender, uint level)
        {
            IsWork = false;
        }

        public void Start(object sender, uint level)
        {
            IsWork = true;

        }

        public void Dispose()
        {
            IsWork = false;
            isThreadCanceled = true;
            imageBackground.Dispose();
        }
    }
}
